package com.sda.Persistence.repository;

import com.sda.Persistence.commons.HibernateUtils;
import com.sda.Persistence.entities.Money;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class MoneyRepository {
    public Money findById(Integer coin_id) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Money type = session.find(Money.class, coin_id);

        session.close();

        return type;
    }

    public List<Money> findByType(Integer coin_type) {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectMoneyByType = session.createQuery("from Money WHERE coin_type = '" + coin_type + "'");

        List<Money> money = selectMoneyByType.list();

        session.close();

        return money;
    }

    public List<Money> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();

        Query selectAllMoney = session.createQuery("from Money");

        List<Money> money = selectAllMoney.list();

        session.close();

        return money;
    }

    public void save(Money money) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(money);

        transaction.commit();
        session.close();
    }

    public void update(Money money) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(money);

        transaction.commit();
        session.close();
    }

    public void deleteById(Money money) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(money);

        transaction.commit();
        session.close();
    }
}
