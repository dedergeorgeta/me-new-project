package com.sda.Persistence.repository;


import com.sda.Persistence.commons.HibernateUtils;
import org.hibernate.Hibernate;
import com.sda.Persistence.entities.Item;
import org.hibernate.Session;

import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ItemRepository {

    public Item findById(Integer item_id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Item item = session.find(Item.class, item_id);

        session.close();
        return item;
    }

    public List<Item> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Query selectAllItemsQuery = session.createQuery("from Item");
        List<Item> items = selectAllItemsQuery.list();

        session.close();
        return items;

    }

    public Item findByName(String itemName) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Query selectItemByNameQuery = session.createQuery("from Item i WHERE i.name = '" + itemName + " '");
        List<Item> names = selectItemByNameQuery.getResultList();

        session.close();
        return names.stream().findAny().get();
    }
    public void save(Item item) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(item);
        transaction.commit();
        session.close();
    }
    public void update(Item item) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(item);

        transaction.commit();
        session.close();
    }

    public void delete(Item item) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(item);

        transaction.commit();
        session.close();
    }
}