package com.sda.Service;

import com.sda.Persistence.entities.Item;
import com.sda.Service.exception.NotFullPaidException;
import com.sda.Service.exception.NotSufficientChangeException;
import com.sda.Service.exception.SoldOutException;

import java.util.List;

public interface IVendingMachine {

    public List<Item> getItems();

    public void insertMoney(Coin money, int quantity);

    public boolean selectProduct(ItemType itemType) throws SoldOutException, NotFullPaidException, NotSufficientChangeException;

    public void purchaseItem(ItemType itemType);
}
