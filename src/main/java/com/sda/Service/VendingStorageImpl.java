package com.sda.Service;

import com.sda.Persistence.entities.Item;
import com.sda.Persistence.entities.Money;
import com.sda.Persistence.repository.ItemRepository;
import com.sda.Persistence.repository.MoneyRepository;
import com.sda.Service.exception.NotFullPaidException;
import com.sda.Service.exception.NotSufficientChangeException;

import java.util.List;

public class VendingStorageImpl implements IVendingStorage {

    private double insertedAmount;
    List<Item> itemList;
    Item item;

    @Override
    public boolean checkInsertedAmount(ItemType itemType) throws NotFullPaidException {
        ItemRepository itemRepository = new ItemRepository();
        item = itemRepository.findByName(itemType.name());
        if (insertedAmount < item.getPrice()) {
            throw new NotFullPaidException();
        }

        return true;
    }

    @Override
    public boolean checkForChange(ItemType itemType) throws NotSufficientChangeException {
        MoneyRepository moneyRepository = new MoneyRepository();
        List<Money> moneys = moneyRepository.findAll();
        double moneyInTheBank = 0;
        Item item = null;
        for (Money money : moneys ) {
            moneyInTheBank += money.getQuantity() * money.getCoin().getValue();
        }
        for (Item i : itemList) {
            if (itemType.name().equals(i.getName())) {
                item = i;
            }
        }
        if (insertedAmount - item.getPrice() > moneyInTheBank ) {
            throw new NotSufficientChangeException();
        }

        return true;
    }

    @Override
    public void releaseItem(ItemType itemType) {
        ItemRepository itemRepository = new ItemRepository();
        Item item = itemRepository.findByName(itemType.name());
        item.setQuantity(item.getQuantity() - 1);
        itemRepository.update(item);

    }

    @Override
    public void addMoney(double value) {
        insertedAmount += value;
    }

    @Override
    public List<Item> getItems() {
        ItemRepository itemRepository = new ItemRepository();
        itemList = itemRepository.findAll();

        return itemList;
    }

}
