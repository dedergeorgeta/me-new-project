package com.sda.Service;

import com.sda.Persistence.entities.Item;
import com.sda.Service.exception.NotFullPaidException;
import com.sda.Service.exception.NotSufficientChangeException;
import javafx.scene.control.Label;

import java.util.List;

public interface IVendingStorage  {

    public boolean checkInsertedAmount(ItemType itemType) throws NotFullPaidException;

    public boolean checkForChange(ItemType itemType) throws NotSufficientChangeException;

    public void releaseItem(ItemType itemType);

    public void addMoney(double value);

    public List<Item> getItems();
}
