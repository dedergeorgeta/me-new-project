package com.sda.Service.exception;

public class SoldOutException extends Exception {
    @Override
    public String getMessage() {
        return "Item not available in stock";
    }
}
